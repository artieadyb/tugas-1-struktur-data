import java.util.LinkedList;

public class App {
    public
     static void main(String[] args) throws Exception {

        //Ini soal no 1
        int nilai = 10;
        System.out.println("ini nilai = "+ nilai);

        //Ini soal no. 2
        String kata = "Struktur";
        System.out.println("ini kata = "+ kata);

        //Ini soal no. 3
        int[] arraySatu = {12,10,40};
        for(int i=0; i<arraySatu.length; i++){
            System.out.println("ini arraySAtu element ke-"+i+" = "+ arraySatu[i]);
        }

        //Ini soal no. 4
        int[][] arrayDua = {
            {12,10,40},
            {13,11,41},
            {14,12,42},
        };
        System.out.println("Ini value arrayDua");
        for(int i=0; i<arrayDua.length; i++){
            for(int j = 0; j<arrayDua[i].length; j++){
                System.out.print(arrayDua[i][j]+"\t");
            }
            System.out.println();
        }

        //Ini soal no. 5
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.add(10);
        linkedList.add(20);
        linkedList.add(30);
        linkedList.add(40);
        linkedList.add(50);

        System.out.println("ini value linkedList");
        for(Integer item : linkedList){
            System.out.println(item);
        };
     }
}
